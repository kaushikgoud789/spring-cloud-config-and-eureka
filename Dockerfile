FROM openjdk:8-jdk-alpine

RUN apk add --no-cache bash curl zip unzip sed

RUN curl -s "https://get.sdkman.io" | bash

RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh && sdk install springboot && sdk use springboot 2.0.4.RELEASE && spring install org.springframework.cloud:spring-cloud-cli:2.0.0.RELEASE"

ENV PATH="/root/.sdkman/candidates/springboot/2.0.4.RELEASE/bin/:${PATH}"

COPY configserver.yml configserver.yml

EXPOSE 8888 8761

ENTRYPOINT ["bin/bash","-c","spring cloud configserver eureka"]
